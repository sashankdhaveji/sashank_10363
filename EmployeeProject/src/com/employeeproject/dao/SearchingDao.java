package com.employeeproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.employeeproject.bean.EmployeeDetails;
import com.employeeproject.util.DBConnection;

/**
 * This class is used to search employee details with given employee id
 * @author Sashank
 *
 */
public class SearchingDao {

/**
 * This method is used to search employee details with given employee id
 * @param id
 * @return Object of employee details
 */
public EmployeeDetails searchById(int id) {
		//Fetching connection object from util package
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		EmployeeDetails details = null;
		String sql = "select * from employee_details where emp_id = ?";
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			//executing the query to fetch employee details
			rs = ps.executeQuery();
			while(rs.next()) {
				details = new EmployeeDetails();
				details.setEmp_id(rs.getInt(1));
				details.setEmp_name(rs.getString(2));
				details.setEmp_age(rs.getInt(3));
				
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return details;
	}
}
