package com.employeeproject.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.employeeproject.bean.EmployeeDetails;
import com.employeeproject.util.DBConnection;
/**
 * This class is used to display exisisting data
 * @author IMVIZAG
 *
 */
public class DisplayEmoployeesDao {
	/**
	 * This method is used to display existing data
	 * @return List of employees
	 */
	public List<EmployeeDetails> displayEmployees()
	{
		//Fetching connection object from util package
		Connection con = DBConnection.getDBConnection();
		ResultSet resultSet=null;
		List<EmployeeDetails> employeeDetails = new ArrayList<EmployeeDetails>();
		try {
			Statement statement = con.createStatement();
			//executing query to fetch all employees
			resultSet = statement.executeQuery("select * from employee_details");
			while(resultSet.next())
			{
				EmployeeDetails details = new EmployeeDetails();
				details.setEmp_name(resultSet.getString(2));
				details.setEmp_id(resultSet.getInt(1));
				details.setEmp_age(resultSet.getInt(3));
				employeeDetails.add(details);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returning the list of employee details
		return employeeDetails;
	}
}
