package com.employeeproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.employeeproject.util.DBConnection;

/**
 * This class is used to insert employee data into database
 * @author Sashank
 *
 */
public class InsertionDao {
	/**
	 * This method is used to insert employee data into database
	 * @param name
	 * @param age
	 * @return true if employee details are inserted successfully
	 */
	public boolean insertion(String name,int age)
	{
		//Fetching connection object from util package
		Connection con = DBConnection.getDBConnection();
		boolean flag=false;
		try {
			 String sql = "insert into employee_details(emp_name,emp_age) values(?,?)";
			 PreparedStatement preparedStatement = con.prepareStatement(sql);
			 preparedStatement.setString(1, name);
			 preparedStatement.setInt(2, age);
			 //executing query to insert an employee to the database
			 int rows = preparedStatement.executeUpdate();
			 if(rows > 0)
			 {
				 flag=true;
			 }
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
}
