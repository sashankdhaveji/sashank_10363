package com.employeeproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.employeeproject.util.DBConnection;
/**
 * This class is used to perform deletion operations
 * @author Sashank
 *
 */
public class DeletionDao {
	/**
	 * This method is used to perform deletion operations
	 * @param id
	 * @return true if employee is deleted successfully
	 */
	public boolean delete(int id)
	{
		//fetching connection variable
		Connection con = DBConnection.getDBConnection();
		boolean flag=false;
		try {
			 String sql = "delete from employee_details where emp_id=?";
			 PreparedStatement preparedStatement = con.prepareStatement(sql);
			 preparedStatement.setInt(1, id);
			 //executing the query
			 int rows = preparedStatement.executeUpdate();
			 if(rows > 0)//checking for a successful deletion operation
			 {
				 flag=true;
			 }
				
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return flag;
	}
	
}
