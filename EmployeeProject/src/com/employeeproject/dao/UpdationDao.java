package com.employeeproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.employeeproject.bean.EmployeeDetails;
import com.employeeproject.util.DBConnection;

/**
 * This class is used to update employee details with given employee details
 * @author Sashank
 *
 */
public class UpdationDao {
	/**
	 * This method is used to check whether given employee id exists or not
	 * @param empID
	 * @return true if employee id exists
	 */
	public boolean employeeExist(int empID)
	{
		boolean flag = false;
		DisplayEmoployeesDao displayEmoployeesDao = new DisplayEmoployeesDao();
		List<EmployeeDetails> employeeDetails = displayEmoployeesDao.displayEmployees();
		for(EmployeeDetails details : employeeDetails)
		{
			//checking for employee id in the database
			if(details.getEmp_id() == empID)
			{
				flag = true;
			}
			
		}
		//returning true if employee exists
		return flag;
	}
	/**
	 * This method is used to update employee details
	 * @param empid
	 * @param empname
	 * @param empage
	 * @return true if updation is successfull
	 */
	public boolean updateEmployeeDetails(int empid,String empname, int empage)
	{
		Connection con = DBConnection.getDBConnection();
		PreparedStatement preparedStatement = null;
		boolean flag = false;
		String sql="update employee_details set emp_name=?,emp_age=? where emp_id=?";
		try {
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(3, empid);
			preparedStatement.setString(1, empname);
			preparedStatement.setInt(2, empage);
			//executing query to update employee details
			int rows = preparedStatement.executeUpdate();
			if(rows>0)
			{
				flag = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
}
