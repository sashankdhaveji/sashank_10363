package com.employeeproject.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeproject.bean.EmployeeDetails;
import com.employeeproject.dao.SearchingDao;

/**
 * Servlet implementation class SearchService
 */
@WebServlet("/SearchService")
public class SearchService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idTemp = request.getParameter("empid");
		int id = Integer.parseInt(idTemp);
		SearchingDao dao = new SearchingDao();
		PrintWriter writer = response.getWriter();
		//calling method to search by employee id  and assigning it to employee object
		EmployeeDetails employee = dao.searchById(id);
		RequestDispatcher dispatcher = request.getRequestDispatcher("search.html");
		dispatcher.include(request, response);
		if(employee == null)
		{
			writer.println("<br><br><h4>Employee Id Not Found</h4>");
		}
		else 
		{
			writer.println("<br><br><table border='2'>");
			writer.println("<tr><th>"+"Employee Id"+"</th><th>Employee Name</th><th>Employee Age</th>");
			writer.println("<tr><td>"+employee.getEmp_id()+"</td><td>"+employee.getEmp_name()+"</td><td>"+employee.getEmp_age()+"</td></tr>");
			writer.println("</table>");
		}
		//closing printwriter
		writer.close();
	}

}
