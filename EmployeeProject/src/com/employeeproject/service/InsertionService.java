package com.employeeproject.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeproject.dao.InsertionDao;

/**
 * Servlet implementation class InsertionService
 */
@WebServlet("/InsertionService")
public class InsertionService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertionService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String ageTemp = request.getParameter("age");
		int age = Integer.parseInt(ageTemp);
		InsertionDao dao = new InsertionDao();
		//calling the method to insert employee details into database
		boolean flag = dao.insertion(name, age);
		
		response.setContentType("text/html");
		PrintWriter printWriter = response.getWriter();
		//employee added
		if(flag)
		{
			RequestDispatcher dispatcher = request.getRequestDispatcher("insertion.html");
			dispatcher.include(request, response);
			printWriter.println("<br><br><h4> Employee added successfully</h4>");
		}
		else
		{
			RequestDispatcher dispatcher = request.getRequestDispatcher("insertion.html");
			dispatcher.include(request, response);
			printWriter.println("<br><br><h4> Employee addition failed</h4>");
		}
		
		//closing printwriter
		printWriter.close();
	}

}
