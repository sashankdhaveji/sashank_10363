package com.employeeproject.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeproject.bean.EmployeeDetails;
import com.employeeproject.dao.DisplayEmoployeesDao;

/**
 * Servlet implementation class DisplayEmployeeService
 */
@WebServlet("/DisplayEmployeeService")
public class DisplayEmployeeService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayEmployeeService() {
        super();
    }
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DisplayEmoployeesDao displayEmoployeesDao = new DisplayEmoployeesDao();
		//calling the method which returns list and displaying that recieved list
		List<EmployeeDetails> employeeDetails = displayEmoployeesDao.displayEmployees();
		
		PrintWriter writer = response.getWriter();
		writer.println("<table border='2'>");
		writer.println("<tr><th>"+"Employee Id"+"</th><th>Employee Name</th><th>Employee Age</th>");
		for(int i = 0;i<employeeDetails.size();i++)
		{
			EmployeeDetails details = employeeDetails.get(i);
			writer.println("<tr><td>"+details.getEmp_id()+"</td><td>"+details.getEmp_name()+"</td><td>"+details.getEmp_age()+"</td></tr>");
		}
		writer.println("</table>");
		//closing printwriter
		writer.close();
	}

}
