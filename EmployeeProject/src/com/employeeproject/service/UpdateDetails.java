package com.employeeproject.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeproject.bean.EmployeeDetails;
import com.employeeproject.dao.SearchingDao;
import com.employeeproject.dao.UpdationDao;

/**
 * Servlet implementation class UpdateService
 */
@WebServlet("/UpdateDetails")
public class UpdateDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idTemp = request.getParameter("id");
		int id = Integer.parseInt(idTemp);
		String name = request.getParameter("name");
		//if employee name feild is left blank then asigning existing name to it
		if(name.equals(""))
		{
			SearchingDao dao = new SearchingDao();
			EmployeeDetails details = dao.searchById(id);
			if(!(details==null))
			{
				name=details.getEmp_name();
			}
		}
		String ageTemp = request.getParameter("age");
		int age=0;
		//if employee age field is left blank then assigning existing age to it
		if(ageTemp.equals(""))
		{
			SearchingDao dao = new SearchingDao();
			EmployeeDetails details =dao.searchById(id);
			if(!(details==null))
			{
				age=details.getEmp_age();
			}
		}
		else
		{
			age = Integer.parseInt(ageTemp);
		}
		UpdationDao dao = new UpdationDao();
		boolean flag = false;
		response.setContentType("text/html");
		PrintWriter printWriter = response.getWriter();
		//calling method to check whether employee id exists or not
		flag = dao.employeeExist(id);
		if(flag)
		{
			//if employee id exists calling method to update employee details
			flag = dao.updateEmployeeDetails(id, name,age);
			if(flag)
			{
				printWriter.println("<br><br><h4> Employee updated successfully</h4>");
				RequestDispatcher dispatcher = request.getRequestDispatcher("updation.html");
				dispatcher.include(request, response);
			}
			else 
			{
				printWriter.println("<br><br><h4> Employee updation failed</h4>");
				RequestDispatcher dispatcher = request.getRequestDispatcher("updation.html");
				dispatcher.include(request, response);
			}
			
		}
		else
		{
			printWriter.println("<br><br><h4> Employee id not found</h4>");
			RequestDispatcher dispatcher = request.getRequestDispatcher("updation.html");
			dispatcher.include(request, response);
		}
		
		//closing printwriter
		printWriter.close();
	}

}
