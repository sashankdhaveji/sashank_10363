package com.onetoonedemo.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static SessionFactory sessionFactory = hibernetConnection();
	private static SessionFactory hibernetConnection() {
		
		ServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
		MetadataSources data = new MetadataSources(registry); 
		Metadata metaData = data.getMetadataBuilder().build();
		
		sessionFactory = metaData.getSessionFactoryBuilder().build();
		
		Session session = sessionFactory.openSession();
		
		return sessionFactory;
		
		
	}
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
