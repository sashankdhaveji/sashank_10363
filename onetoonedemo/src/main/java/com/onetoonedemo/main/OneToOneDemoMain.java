package com.onetoonedemo.main;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.onetoonedemo.entity.DrivingLisence;
import com.onetoonedemo.entity.Person;
import com.onetoonedemo.util.HibernateUtil;

public class OneToOneDemoMain {

	public static void main(String[] args) 
	{
		
		Person person = new Person();
		person.setPerson_name("Varun");
		
		DrivingLisence drivingLisence = new DrivingLisence();
		drivingLisence.setPerson(person);
		person.setDrivingLisence(drivingLisence);
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session =  sessionFactory.openSession();

		session.save(person);
		session.save(drivingLisence);
		
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		
		Person personfetch = session.get(Person.class, 1);
		DrivingLisence drivingLisencefetch = session.get(DrivingLisence.class, 1);
		
		System.out.println("Id: "+personfetch.getPerson_Id()+"\nName: "+personfetch.getPerson_name());
		System.out.println("Driving Licence:"+drivingLisencefetch.getDrivinglicence_id());
	}

}
