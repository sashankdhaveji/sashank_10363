package com.onetoonedemo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table
public class Person {
	@Id
	@Column
	@GeneratedValue(generator="chandu")
	@GenericGenerator(name="chandu",strategy="foreign",parameters={@Parameter(name="property",value="drivingLisence")})
	private int person_Id;
	@Column
	private String person_name;
	@OneToOne(cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private DrivingLisence drivingLisence;
	
	@Override
	public String toString() {
		return "Person [person_Id=" + person_Id + ", person_name=" + person_name + ", drivingLisence=" + drivingLisence
				+ "]";
	}
	public int getPerson_Id() {
		return person_Id;
	}
	public void setPerson_Id(int person_Id) {
		this.person_Id = person_Id;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public DrivingLisence getDrivingLisence() {
		return drivingLisence;
	}
	public void setDrivingLisence(DrivingLisence drivingLisence) {
		this.drivingLisence = drivingLisence;
	}
}
