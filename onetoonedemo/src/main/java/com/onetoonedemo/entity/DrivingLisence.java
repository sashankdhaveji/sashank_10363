package com.onetoonedemo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table
public class DrivingLisence {
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int drivinglicence_id;
	@Override
	public String toString() {
		return "DrivingLisence [drivinglicence_id=" + drivinglicence_id + ", person=" + person + "]";
	}
	@OneToOne(mappedBy="drivingLisence",cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Person person;
	
	public int getDrivinglicence_id() {
		return drivinglicence_id;
	}
	public void setDrivinglicence_id(int drivinglicence_id) {
		this.drivinglicence_id = drivinglicence_id;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
		
}
