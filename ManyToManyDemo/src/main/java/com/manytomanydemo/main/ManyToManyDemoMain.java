package com.manytomanydemo.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.manytomanydemo.entity.Client;
import com.manytomanydemo.entity.Company;


public class ManyToManyDemoMain {

	public static void main(String[] args) {
		
		
		Client client1 = new Client();
		client1.setClientName("Microsoft");
		client1.setAmount(50000);
		
		Client client2 = new Client();
		client2.setClientName("infotech");
		client2.setAmount(80000);
		
		Company company1 = new Company();
		company1.setCompanyName("aspire");
		
		Company company2 = new Company();
		company2.setCompanyName("Innominds");
		
		SessionFactory sessionFactory = com.manytomanydemo.util.HibernateUtil.getSessionFactory();
		Session session =  sessionFactory.openSession();

		session.save(company1);
		session.save(client1);
		
		session.save(company2);
		session.save(client2);
		
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		
		Company company = session.get(Company.class, 1);
		Client client = session.get(Client.class, 1);
		Company companydetails = session.get(Company.class, 2);
		Client clientdetails = session.get(Client.class, 2);
		
		System.out.println("company1 Id: "+company.getCmpnyId()+"\nCompany1 Name: "+company.getCompanyName());
		System.out.println("client1 Id:"+client.getClientId()+"\nClient1 Name: "+client.getClientName());
		
		System.out.println("company2 Id: "+companydetails.getCmpnyId()+"\nCompany2 Name: "+companydetails.getCompanyName());
		System.out.println("client2 Id:"+clientdetails.getClientId()+"\nClient2 Name: "+clientdetails.getClientName());
		
	}

}
