package com.manytomanydemo.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "client_tab")
public class Client {

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int clientId;
	@Column
	private String clientName;
	@Column
	private double amount;
	
	@ManyToMany
	@JoinTable(name = "manytomany",  joinColumns = { @JoinColumn(name = "clientId") },   
            inverseJoinColumns = { @JoinColumn(name = "cmpnyId") })
	private List<Company> company;
	
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
