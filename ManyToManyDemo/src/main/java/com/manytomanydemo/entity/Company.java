package com.manytomanydemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "company_tab")
public class Company {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cmpnyId;
	@Column
	private String companyName;
	@Column
	private int noOfClients;
	
	
	public int getCmpnyId() {
		return cmpnyId;
	}
	public void setCmpnyId(int cmpnyId) {
		this.cmpnyId = cmpnyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getNoOfClients() {
		return noOfClients;
	}
	public void setNoOfClients(int noOfClients) {
		this.noOfClients = noOfClients;
	}
	

	
}
