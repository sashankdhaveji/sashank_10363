package com.sample.service;

import java.util.List;

import com.sample.bean.Student;

/**
 * This is the interface in which all the abstract classes are defined such as
 * insertion, searching, displaying, deletion and updation
 * 
 * @author IMVIZAG
 *
 */

public interface StudentService {
	
	boolean insertStudent(Student student);
	
	Student findById(int id);
	
	List<Student> fetchAllStudents();
	
	boolean removeStudent(int id);
	
	boolean updateStudent(Student student);
}
