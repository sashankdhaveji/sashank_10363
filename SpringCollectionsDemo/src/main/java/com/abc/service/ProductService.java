package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.dao.ProductDao;
import com.abc.entity.Product;

@Service
public class ProductService {
	//taking productdao object from spring
	@Autowired
	ProductDao productDao;

	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}
	
	public boolean saveProduct(List<Product> productList)
	{
		return this.productDao.saveProduct(productList);//calling method to save product in dao
	}
}
