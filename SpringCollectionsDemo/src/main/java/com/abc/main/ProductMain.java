package com.abc.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.controller.ProductController;
import com.abc.entity.Product;
import com.abc.entity.ProductList;

public class ProductMain {
	public static void main(String[] args) {
		//getting product controller object from spring
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/abc/springconfig/config.xml");
		ProductController productController = context.getBean(ProductController.class);
		//getting product object from spring
		ProductList productList = (ProductList) context.getBean("productL");
		if(productController.saveProduct(productList.getProducts()))//calling method to save product object in database
		{
			System.out.println("Products Saved");
		}
		else
		{
			System.out.println("Product Saving failed");
		}
		((AbstractApplicationContext) context).close();
	}
	
}
