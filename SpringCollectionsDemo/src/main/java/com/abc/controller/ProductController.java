package com.abc.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.abc.entity.Product;
import com.abc.service.ProductService;

@Controller
public class ProductController {
	//Taking product service object from spring
	@Autowired
	ProductService productService;
	
	
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	
	public boolean saveProduct(List<Product> productList)
	{
		return productService.saveProduct(productList);//calling method in productservice class
	}
	
}
