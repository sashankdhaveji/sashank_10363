package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServieImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;
		
	@Transactional
	@Override
	public Account searchAccountByID(int accno) {
		return accountDAO.getAccountByID(accno);
	}

	@Transactional
	@Override
	public int insertAccount(Account account) {
		if(account.getName()==null)
		{
			return 0;
		}
		return accountDAO.insertAccount(account);
	}

	@Transactional
	@Override
	public boolean deleteAccount(int accno) {
		Account account = accountDAO.getAccountByID(accno);
		if(account==null)
		{
			return false;
		}
		else
		{
			accountDAO.deleteAccount(account);
			return true;
		}
	}

	@Transactional
	@Override
	public boolean updateAccount(Account account) {
		Account accountSerach = accountDAO.getAccountByID(account.getAccno());
		if(accountSerach==null)
		{
			return false;
		}
		else
		{
			
			if(account.getName()!=null)
			{
				accountSerach.setName(account.getName());
			}
			if(account.getBalance()!=0.0)
			{
				accountSerach.setBalance(account.getBalance());
			}			
			accountDAO.updateAccount(accountSerach);
			return true;
		}
		
	}

	@Transactional
	@Override
	public List<Account> getAllAccounts() {
		return accountDAO.getAllAccounts();
	}

}
