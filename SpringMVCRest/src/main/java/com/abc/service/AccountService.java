package com.abc.service;

import java.util.List;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	/**
	 * This method is used to fetch account object from database 
	 * @param accno
	 * @return Account object
	 */
	Account searchAccountByID(int accno);
	
	/**
	 * This method is used to insert an account into database
	 * @param account
	 * @return true if updated
	 */
	int insertAccount(Account account);
	
	/**
	 * This method is used to delete an account from database
	 * @param accno
	 * @return true if updated
	 */
	boolean deleteAccount(int accno);
	
	/**
	 * This method is used to update existing account's details
	 * @param account
	 * @return true if updated
	 */
	boolean updateAccount(Account account);
	
	/**
	 * This method is used to fetch all account details from database
	 * @return list of all accounts
	 */
	List<Account> getAllAccounts();
}
