package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@RestController
/**
 * This class is used to provide implementations for restful web services
 * @author Sashank
 *
 */
public class AccountController {
	
	//Fetching Service object using spring framework auto wired feature
	@Autowired
	private AccountService accountService;
	
	/**
	 * This clas is used to search an account by given account number 
	 * @param accno
	 * @return Account object
	 */
	@GetMapping("accountsearch/{id}")
	public ResponseEntity<Account> searchAccount(@PathVariable("id") int accno) 
	{
		Account account = accountService.searchAccountByID(accno);
		if(account==null)
		{
			return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);		
		}
		else
		{
			return new ResponseEntity<Account>(account,HttpStatus.OK);
		}
		
	}
	
	/**
	 * This account is used to insert an account into database
	 * @param account
	 * @return Message which is displayed
	 */
	@PostMapping("accountsinsertion")
	public ResponseEntity<?> insertAccount(@RequestBody Account account) 
	{
		int accno=accountService.insertAccount(account);
		if(accno==0)
		{
			return ResponseEntity.ok().body("Account insertion failed");	
		}
		return ResponseEntity.ok().body("Account Saved!Your account number is:"+accno);		
	}
	
	/**
	 * This method is used to delete an account by taking an account number
	 * @param accno
	 * @return Message which is displayed
	 */
	@DeleteMapping("accountsdelete/{id}")
	public ResponseEntity<?> deleteAccount(@PathVariable("id") int accno) 
	{
		if(accountService.deleteAccount(accno))
		{
			return ResponseEntity.ok().body("Account Deleted");
		}
		else
		{
			return ResponseEntity.ok().body("Account Deletion Failed");
		}
		
	}
	
	/**
	 * This method is used to update existing account details
	 * @param account
	 * @return Message which is displayed
	 */
	@PutMapping("accountUpdate")
	public ResponseEntity<?> updateAccount(@RequestBody Account account) {		
		
		if(accountService.updateAccount(account)) {
			return ResponseEntity.ok().body("Account updated");
		}
		else {
			return ResponseEntity.ok().body("Account updation Failed");
		}
	}
	
	/**
	 * This method is used to dispaly all existing accounts
	 * @return List of all accounts
	 */
	@GetMapping("getAllAccounts")
	public ResponseEntity<List<Account>> getAllAccounts()
	{
		List<Account> accounts = accountService.getAllAccounts();
		if(accounts.size()>0)
		{
			return new ResponseEntity<List<Account>>(accounts,HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity("Accounts dosn't exist", null);
		}
	}
}