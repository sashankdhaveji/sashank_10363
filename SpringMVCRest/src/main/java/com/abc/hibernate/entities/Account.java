package com.abc.hibernate.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name= "account_tbl")
public class Account {

	@Id
	@Column(name="accno")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int accno;
	@Column(name="name",nullable=false)
	private String name;
	@Column(name="balance",nullable=false)
	private double balance;
	public int getAccno() {
		return accno;
	}
	public void setAccno(int accno) {
		this.accno = accno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
}
