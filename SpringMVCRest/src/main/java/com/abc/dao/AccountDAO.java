package com.abc.dao;

import java.util.List;

import com.abc.hibernate.entities.Account;

/**
 * This interface is used to provide definition to all dao methods
 * @author Sashank
 *
 */
public interface AccountDAO {
	/**
	 * This method is used to fetch an account object from database by given account number
	 * @param accno
	 * @return Account
	 */
	Account getAccountByID(int accno);
	
	/**
	 * This method is used to insert an account into the database
	 * @param account
	 * @return account number
	 */
	int insertAccount(Account account);
	
	/**
	 * This account is used to delete an account from database by given account number
	 * @param account
	 */
	void deleteAccount(Account account);
	
	/**
	 * This account is used to update account details existing in the database
	 * @param account
	 */
	void updateAccount(Account account);
	
	/**
	 * This method is used to fetch all accounts 
	 * @return list of all accounts
	 */
	List<Account> getAllAccounts();
}
