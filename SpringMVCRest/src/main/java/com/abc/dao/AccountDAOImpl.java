package com.abc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {
	
	//Fetching the sessionfactory object provided by spring framework
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Account getAccountByID(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}

	@Override
	public int insertAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return account.getAccno();
	}

	@Override
	public void deleteAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		
		
		session.delete(account);
	}

	@Override
	public void updateAccount(Account account) {
		
		Session session = sessionFactory.getCurrentSession();
		
		session.update(account);
		
		
	}

	@Override
	public List<Account> getAllAccounts() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Account.class);
		return criteria.list();
	}

	
}
