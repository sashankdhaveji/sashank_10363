package com.springdemoannotations.javaconfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.springdemo.bean.EmployeeAddress;
import com.springdemo.bean.EmployeeBean;


@Configuration
/**
 * This class is used to provide configurations to implement spring framework
 * @author Sashank
 *
 */
public class ApplicationConfiguration {
	
	
	@Bean
	public EmployeeBean getEmployeeBean()
	{
		//creating object and setting the values for them
		EmployeeBean employeeBean = new EmployeeBean();
		employeeBean.setEmpId(1);
		employeeBean.setEmpName("Sashank");
		EmployeeAddress empAddress = new EmployeeAddress();
		empAddress.setCity("hyd");
		empAddress.setState("TS");
		employeeBean.setEmpAddress(empAddress);
		return employeeBean;
	}
	
}