<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<c:if test="${listcase }">
<table border="2">
<tr><th>Employee name</th><th>Employee ID</th><th>Employee age</th><th>Employee city</th><th>Employee state</th><th>Employee country</th></tr>
<c:forEach var="employee" items="${empdetails}">
<tr>
        <td>${employee.emp_name}</td>
        <td>${employee.emp_id}</td>
        <td>${employee.emp_age}</td>
        <td>${employee.address.city}</td>
        <td>${employee.address.state}</td>
        <td>${employee.address.country}</td>
        </tr>
</c:forEach>
</table>
</c:if>
<c:if test="${!listcase }"><h2>There are no employees to display!!</h2></c:if>
</body>
</html>