<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2 align="center">Employee Deletion</h2>
<form action="DeletionServlet" method="post">
Enter employee ID:<input type="number" min="101" name="empid" maxlength="4" required="required"><br><br>
<input type="submit" value="delete">
</form>
<c:if test="${deletioncase }">
<h2>Employee Deleted Successfully</h2>
</c:if>
<c:if test="${!deletioncase && count==0}">
<h2>Employee Deletion Failed</h2>
</c:if>
</body>
</html>