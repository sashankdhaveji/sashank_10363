package com.employeeprojectjsp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.employeeprojectjsp.dao.DeletionEmployeesDao;

/**
 * Servlet implementation class DeletionServlet
 */
@WebServlet("/DeletionServlet")
public class DeletionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		if(session != null)
		{
			if(session.getAttribute("username") != null)
			{
				int count = 0;
				String idTemp = request.getParameter("empid");
				int id = Integer.parseInt(idTemp);
				
				DeletionEmployeesDao dao = new DeletionEmployeesDao();
				//calling the method to delete employee
				boolean flag = dao.deletion(id);
				
				request.setAttribute("deletioncase", flag);
				request.setAttribute("count", count);
				RequestDispatcher dispatcher = request.getRequestDispatcher("Delete.jsp");
				dispatcher.include(request, response);
			}
			else
			{
				response.sendRedirect("Sessionexpired.html");
			}
			
		}
		else
		{
			response.sendRedirect("Sessionexpired.html");
		}
		
	}

}
