package com.employeeprojectjsp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.employeeprojectjsp.bean.EmployeeDetails;
import com.employeeprojectjsp.dao.DisplayEmoployeesDao;

/**
 * Servlet implementation class DisplayEmployees
 */
@WebServlet("/DisplayEmployees")
public class DisplayEmployees extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session=request.getSession(false);
		if(session != null)
		{
			if(session.getAttribute("username") != null)
			{
				DisplayEmoployeesDao displayEmoployeesDao = new DisplayEmoployeesDao();
				//calling the method which returns list and displaying that recieved list
				List<EmployeeDetails> employeeDetails = displayEmoployeesDao.displayEmployees();
				if(employeeDetails.isEmpty())
				{
					System.out.println(employeeDetails);
					request.setAttribute("listcase", false);
				}
				else
				{
					request.setAttribute("listcase", true);
				}
				request.setAttribute("empdetails", employeeDetails);
				RequestDispatcher dispatcher = request.getRequestDispatcher("Display.jsp");
				dispatcher.include(request, response);
			}
			
			else
			{
				response.sendRedirect("Sessionexpired.html");
			}
		}
		else
		{
			response.sendRedirect("Sessionexpired.html");
		}
		
	}

}
