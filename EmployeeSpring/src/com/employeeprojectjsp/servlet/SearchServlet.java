package com.employeeprojectjsp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.employeeprojectjsp.bean.EmployeeDetails;
import com.employeeprojectjsp.dao.DisplayEmoployeesDao;
import com.employeeprojectjsp.dao.SearchingDao;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session=request.getSession(false);
		if(session != null)
		{
			if(session.getAttribute("username") != null)
			{
				String idTemp = request.getParameter("empid");
				int id = Integer.parseInt(idTemp);
				int count = 0;
				boolean flag = false;
				
				DisplayEmoployeesDao displayEmoployeesDao = new DisplayEmoployeesDao();
				List<EmployeeDetails> employeeDetails = displayEmoployeesDao.displayEmployees();
				
				for(EmployeeDetails employeeDetails2 : employeeDetails)
				{
					if(id == employeeDetails2.getEmp_id())
					{
						flag=true;
					}
				}
				
				EmployeeDetails details=null;
				if(flag)
				{
					SearchingDao dao = new SearchingDao();
					details = dao.searchById(id);
				}
				request.setAttribute("count", count);
				request.setAttribute("employee", details);
				request.setAttribute("searchcase", flag);
				RequestDispatcher dispatcher = request.getRequestDispatcher("Search.jsp");
				dispatcher.include(request, response);
			}
			else
			{
				response.sendRedirect("Sessionexpired.html");
			}
			
		}
		else
		{
			response.sendRedirect("Sessionexpired.html");
		}
		
	}

}
