package com.employeeprojectjsp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.employeeprojectjsp.dao.InsertionDao;
import com.employeeprojectjsp.bean.EmployeeAddress;
import com.employeeprojectjsp.bean.EmployeeDetails;

/**
 * Servlet implementation class InsertionServlet
 */
@WebServlet("/InsertionServlet")
public class InsertionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		if(session != null)
		{
			if(session.getAttribute("username") != null)
			{
				String name = request.getParameter("name");
				String ageTemp = request.getParameter("age");
				int age = Integer.parseInt(ageTemp);
				String city = request.getParameter("city");
				String state = request.getParameter("state");
				String country = request.getParameter("country");
				EmployeeAddress address = new EmployeeAddress();
				address.setCity(city);
				address.setState(state);
				address.setCountry(country);
				EmployeeDetails employeeDetails = new EmployeeDetails();
				employeeDetails.setEmp_name(name);
				employeeDetails.setEmp_age(age);
				InsertionDao dao = new InsertionDao();
				//calling the method to insert employee details into database
				boolean flag = dao.insertion(employeeDetails, address);
				request.setAttribute("empadd", flag);
				RequestDispatcher dispatcher = request.getRequestDispatcher("Insertion.jsp");
				dispatcher.include(request, response);
				int count=0;
				request.setAttribute("count", count);
			}
			else
			{
				response.sendRedirect("Sessionexpired.html");
			}
					
		}
		else {
			response.sendRedirect("Sessionexpired.html");
		}
	}

}
