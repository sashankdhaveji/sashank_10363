<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2 align="center">Employee Details Searching</h2>
<form action="SearchServlet" method="post">
Enter employee ID:<input type="number" min="101" name="empid" maxlength="4" required><br><br>
<input type="submit" value="search">
</form>
<c:if test="${searchcase }">
<br><br><table border="2">
<tr><th>Employee name</th><th>Employee ID</th><th>Employee age</th><th>Employee city</th><th>Employee state</th><th>Employee country</th></tr>
		<tr>
        <td>${employee.emp_name}</td>
        <td>${employee.emp_id}</td>
        <td>${employee.emp_age}</td>
        <td>${employee.address.city}</td>
        <td>${employee.address.state}</td>
        <td>${employee.address.country}</td>
        </tr>
</table>
</c:if>
<c:if test="${!searchcase && count==0}">
<h3>Employee Id Not Found</h3>
</c:if>
</body>
</html>