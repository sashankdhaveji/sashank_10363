package com.employeeprojectjsp.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	int logincount = 0;
	boolean logincase;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("pwd");
		HttpSession session = request.getSession();
		session.setAttribute("username", username);
		session.setAttribute("logincount", logincount);
		logincount = (int) session.getAttribute("logincount");
		if(username.equals("admin") && password.equals("admin123"))
		{
			logincase=false;
			RequestDispatcher dispatcher = request.getRequestDispatcher("Employee.jsp");
			dispatcher.include(request, response);
			session.setMaxInactiveInterval(5*60);
		}
		else
		{
			logincase = true;
			logincount++;
			session.setAttribute("logincount", logincount);
			RequestDispatcher dispatcher = request.getRequestDispatcher("Login.jsp");
			dispatcher.include(request, response);
			
		}
		if(logincount >2 && session!=null)
		{
			RequestDispatcher dispatcher = request.getRequestDispatcher("Error.html");
			logincount=0;
			dispatcher.forward(request, response);
			session.invalidate();
		}
		request.setAttribute("logincase", logincase);
		request.setAttribute("logincount", logincount);
	}

}
