package com.employeeprojectjsp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.employeeprojectjsp.bean.EmployeeAddress;
import com.employeeprojectjsp.bean.EmployeeDetails;
import com.employeeprojectjsp.util.DBConnection;

/**
 * This class is used to search employee details with given employee id
 * @author Sashank
 *
 */
public class SearchingDao {

/**
 * This method is used to search employee details with given employee id
 * @param id
 * @return Object of employee details
 */
public EmployeeDetails searchById(int id) {
		//Fetching connection object from util package
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		EmployeeDetails details = null;
		EmployeeAddress address = null;
		String sqlemployee = "select * from employee_details_jsp where emp_id = ?";
		String sqladdress = "select * from address_employee_jsp where addressid = ?";
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(sqlemployee);
			ps.setInt(1, id);
			//executing the query to fetch employee details
			rs = ps.executeQuery();
			while(rs.next()) {
				details = new EmployeeDetails();
				details.setEmp_id(rs.getInt(1));
				details.setEmp_name(rs.getString(2));
				details.setEmp_age(rs.getInt(3));
			}
			
			ps = con.prepareStatement(sqladdress);
			ps.setInt(1, id);
			//executing the query to fetch employee details
			rs = ps.executeQuery();
			while(rs.next()) {
				address = new EmployeeAddress();
				address.setCity(rs.getString(1));
				address.setState(rs.getString(2));
				address.setCountry(rs.getString(3));
			}
			details.setAddress(address);
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return details;
	}
}
