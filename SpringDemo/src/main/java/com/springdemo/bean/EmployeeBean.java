package com.springdemo.bean;

public class EmployeeBean {
	//declaring fields required for the bean class
	private int empId;
	private String empName;
	private EmployeeAddress empAddress;
	
	//Getters and setters for the fields
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public EmployeeAddress getEmpAddress() {
		return empAddress;
	}
	public void setEmpAddress(EmployeeAddress empAddress) {
		this.empAddress = empAddress;
	}
	
}
