<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<c:if test="${registerBean.password1 eq registerBean.password2 }">
<h2>Registration Success</h2> 
First Name : ${registerBean.first_name }<br>
Last Name : ${registerBean.last_name }<br>
Age : ${registerBean.age }<br>
Email : ${registerBean.email }<br>
</c:if>
<c:if test="${registerBean.password1 ne registerBean.password2 }">
<h2>Registration Failure</h2>
</c:if>
</body>
</html>