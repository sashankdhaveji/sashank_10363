package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	@RequestMapping("/accountsearch/{id}")
	public String searchAccount(@PathVariable("id") int accno,ModelMap map) 
	{
		boolean flag;
		Account account = accountService.searchAccountByID(accno);
		if(account==null)
		{
			flag=false;
		}
		else
		{
			flag = true;
		}
		map.addAttribute("search",flag);
		map.addAttribute("account", account);
		return "account_details";
	}
	
	@GetMapping("/getInsertion")
	public String getInsertion()
	{
		return "Insertion";
	}
	
	@PostMapping("/accountsinsertion")
	public String insertAccount(@ModelAttribute Account account,ModelMap map) 
	{
		boolean flag= accountService.insertAccount(account);
		map.addAttribute("insertion", flag);
		return "account_saved";
	}
	
	@RequestMapping("/accountsdelete/{id}")
	public String deleteAccount(@PathVariable("id") int accno,ModelMap map) 
	{
		boolean flag= accountService.deleteAccount(accno);
		map.addAttribute("delete", flag);
		return "account_delete";
	}
	
	@GetMapping("/getUpdation")
	public String getUpdation()
	{
		return "Update";
	}
	@PostMapping("/accountUpdate")
	public String updateAccount(@ModelAttribute Account account,ModelMap map) {
		
		
		
		boolean flag;
		
		if(account!=null) {
			 flag = accountService.updateAccount(account);
		}
		else {
			flag = false;
		}
		map.addAttribute("update", flag);
		
		return  "account_update";
	}
	
	@GetMapping("/getAllAccounts")
	public String getAllAccounts(ModelMap map)
	{
		List<Account> accounts = accountService.getAllAccounts();
		map.addAttribute("accounts",accounts);
		return "account_allaccounts";
	}
}








