package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServieImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;
		
	@Transactional
	@Override
	public Account searchAccountByID(int accno) {
		return accountDAO.getAccountByID(accno);
	}

	@Transactional
	@Override
	public boolean insertAccount(Account account) {
		return accountDAO.insertAccount(account);
	}

	@Transactional
	@Override
	public boolean deleteAccount(int accno) {
		return accountDAO.deleteAccount(accno);
	}

	@Transactional
	@Override
	public boolean updateAccount(Account account) {


		return accountDAO.updateAccount(account);
	}

	@Transactional
	@Override
	public List<Account> getAllAccounts() {
		return accountDAO.getAllAccounts();
	}

}
