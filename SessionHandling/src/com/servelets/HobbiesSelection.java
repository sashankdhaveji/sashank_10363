package com.servelets;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/HobbiesSelection")
public class HobbiesSelection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<String> string;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//creating the session for using the existing session
		HttpSession session = request.getSession(false);
		if(session == null)
		{
			session = request.getSession();
			string=new ArrayList<String>();	
		}
		else
		{
			//Taking the list from session
			string = (List<String>) session.getAttribute("hobbies");
		}
		//storing the input into a string array
		String hobbise [] = request.getParameterValues("hobbies");
		if(hobbise.length == 0)
		{
			RequestDispatcher dispatcher = request.getRequestDispatcher("shop.html");
			dispatcher.include(request, response);
			PrintWriter printWriter = response.getWriter();
			printWriter.println("<br><br><h4>please select at least one option</h4>");
		}
		
		//traversing the list to add the choice into list
		for(String hobby:hobbise) {
			string.add(hobby);
		}
		//setting the session attribute
		session.setAttribute("hobbies", string);
		
		//creating the session output including the request and response
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Display");
		dispatcher.include(request, response);
		
	}

}
