package com.springdemo.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.springdemo.bean.EmployeeBean;

public class SpringDemoMain {

	public static void main(String[] args) {
		
		//Getting the object using applicationcotext
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/springdemo/config/config.xml");
		//Getting the object using bean Id
		EmployeeBean employeeBean = (EmployeeBean) context.getBean("emp");
		System.out.println("ID: " + employeeBean.getEmpId());
		System.out.println("Name: " + employeeBean.getEmpName());
		System.out.println("City: " + employeeBean.getEmpAddress().getCity());
		System.out.println("State: " + employeeBean.getEmpAddress().getState());
		((AbstractApplicationContext) context).close();
	}

}