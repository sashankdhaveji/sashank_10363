<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employee Insertion</title>
</head>
<body>
<h2 align="center">Employee insertion</h2>
<form action="InsertionServlet" method="post">
Enter name: <input type="text" name="name" pattern="[a-zA-z]+" title="Enter only alphabets" required="required"><br><br>
Enter age: <input type="number" name="age" min="20" maxlength="2" title="Enter only numbers" required="required"><br><br>
Enter city: <input type="text" name="city" pattern="[a-zA-z]+" title="Enter only alphabets" required="required"><br><br>
Enter state: <input type="text" name="state" pattern="[a-zA-z]+" title="Enter only alphabets" required="required"><br><br>
Enter country: <input type="text" name="country" pattern="[a-zA-z]+" title="Enter only alphabets" required="required"><br><br>
<input type = "submit" value="Add employee">
</form>
</body>
<c:if test = "${empadd }">
<h2>Employee Addition Successfull</h2>
</c:if>
<c:if test = "${!empadd }">
<h2>Employee Addition failed</h2>
</c:if>
</html>