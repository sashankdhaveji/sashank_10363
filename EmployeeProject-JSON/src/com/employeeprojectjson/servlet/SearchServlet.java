package com.employeeprojectjson.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeprojectjson.bean.EmployeeDetails;
import com.employeeprojectjson.dao.DisplayEmoployeesDao;
import com.employeeprojectjson.dao.SearchingDao;
import com.google.gson.Gson;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idTemp = request.getParameter("empid");
		int id = Integer.parseInt(idTemp);
		
		boolean flag = false;
		
		DisplayEmoployeesDao displayEmoployeesDao = new DisplayEmoployeesDao();
		List<EmployeeDetails> employeeDetails = displayEmoployeesDao.displayEmployees();
		
		for(EmployeeDetails employeeDetails2 : employeeDetails)
		{
			if(id == employeeDetails2.getEmp_id())
			{
				flag=true;
			}
		}
		
		EmployeeDetails details=null;
		if(flag)
		{
			SearchingDao dao = new SearchingDao();
			details = dao.searchById(id);
		}
		
		String employee = gson.toJson(details);
		PrintWriter writer = response.getWriter();
		writer.println(employee);
	}

}
