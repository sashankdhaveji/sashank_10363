package com.employeeprojectjson.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeprojectjson.bean.EmployeeDetails;
import com.employeeprojectjson.dao.DisplayEmoployeesDao;
import com.google.gson.Gson;

/**
 * Servlet implementation class DisplayEmployees
 */
@WebServlet("/DisplayEmployees")
public class DisplayEmployees extends HttpServlet {
	private Gson gson = new Gson();
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DisplayEmoployeesDao displayEmoployeesDao = new DisplayEmoployeesDao();
		//calling the method which returns list and displaying that recieved list
		List<EmployeeDetails> employeeDetails = displayEmoployeesDao.displayEmployees();
		
		String empdetails = gson.toJson(employeeDetails);
		PrintWriter printWriter = response.getWriter();
		printWriter.println(empdetails);
	}

}
