package com.employeeprojectjson.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeprojectjson.bean.EmployeeAddress;
import com.employeeprojectjson.bean.EmployeeDetails;
import com.employeeprojectjson.dao.InsertionDao;

/**
 * Servlet implementation class InsertionServlet
 */
@WebServlet("/InsertionServlet")
public class InsertionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String ageTemp = request.getParameter("age");
		int age = Integer.parseInt(ageTemp);
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String country = request.getParameter("country");
		EmployeeAddress address = new EmployeeAddress();
		address.setCity(city);
		address.setState(state);
		address.setCountry(country);
		EmployeeDetails employeeDetails = new EmployeeDetails();
		employeeDetails.setEmp_name(name);
		employeeDetails.setEmp_age(age);
		InsertionDao dao = new InsertionDao();
		//calling the method to insert employee details into database
		boolean flag = dao.insertion(employeeDetails, address);
		request.setAttribute("empadd", flag);
		RequestDispatcher dispatcher = request.getRequestDispatcher("Insertion.jsp");
		dispatcher.include(request, response);
	}

}
