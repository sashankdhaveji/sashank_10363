package com.employeeprojectjson.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.employeeprojectjson.bean.EmployeeAddress;
import com.employeeprojectjson.bean.EmployeeDetails;
import com.employeeprojectjson.util.DBConnection;

public class DeletionEmployeesDao {
	
	public boolean deletion(int empid)
	{
		Connection con = DBConnection.getDBConnection();
		boolean flag = false;
		int count = 0;
		DisplayEmoployeesDao emoployeesDao = new DisplayEmoployeesDao();
		List<EmployeeDetails> details = emoployeesDao.displayEmployees();
		for(EmployeeDetails details2 : details)
		{
			if(details2.getEmp_id() == empid)
			{				
				count++;
			}
		}
		if(count==0)
		{
			return false;
		}
		try {
			 String sqlemployee = "delete from employee_details_jsp where emp_id=?";
			 PreparedStatement preparedStatement = con.prepareStatement(sqlemployee);
			 preparedStatement.setInt(1, empid);
			 //executing the query
			 preparedStatement.executeUpdate();
			
			 String sqladdress = "delete from address_employee_jsp where addressid=?";
			 preparedStatement = con.prepareStatement(sqladdress);
			 preparedStatement.setInt(1, empid);
			 //executing the query
			 int rows = preparedStatement.executeUpdate();
				
			
			 if(rows > 0)//checking for a successful deletion operation
			 {
				 flag=true;
			 }
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return flag;
	}
	
}
