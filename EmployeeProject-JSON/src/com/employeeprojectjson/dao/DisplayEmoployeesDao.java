package com.employeeprojectjson.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.employeeprojectjson.bean.EmployeeAddress;
import com.employeeprojectjson.bean.EmployeeDetails;
import com.employeeprojectjson.util.DBConnection;
/**
 * This class is used to display exisisting data
 * @author IMVIZAG
 *
 */
public class DisplayEmoployeesDao {
	/**
	 * This method is used to display existing data
	 * @return List of employees
	 */
	public List<EmployeeDetails> displayEmployees()
	{
		//Fetching connection object from util package
		Connection con = DBConnection.getDBConnection();
		ResultSet resultSet=null;
		List<EmployeeDetails> employeeDetails = new ArrayList<EmployeeDetails>();
		try {
			Statement statement = con.createStatement();
			//executing query to fetch all employees
			resultSet = statement.executeQuery("select e.emp_id,e.emp_name,e.emp_age,a.city,a.state,a.country  from employee_details_jsp e inner join address_employee_jsp a where e.emp_address=a.addressid;");
			while(resultSet.next())
			{
				EmployeeDetails details = new EmployeeDetails();
				EmployeeAddress address = new EmployeeAddress();
				details.setEmp_name(resultSet.getString(2));
				details.setEmp_id(resultSet.getInt(1));
				details.setEmp_age(resultSet.getInt(3));
				address.setCity(resultSet.getString(4));
				address.setState(resultSet.getString(5));
				address.setCountry(resultSet.getString(6));
				details.setAddress(address);
				employeeDetails.add(details);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returning the list of employee details
		return employeeDetails;
	}
}
