package com.employeeprojectjson.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.employeeprojectjson.bean.EmployeeAddress;
import com.employeeprojectjson.bean.EmployeeDetails;
import com.employeeprojectjson.util.*;

/**
 * This class is used to insert employee data into database
 * @author Sashank
 *
 */
public class InsertionDao {
	/**
	 * This method is used to insert employee data into database
	 * @param name
	 * @param age
	 * @return true if employee details are inserted successfully
	 */
	public boolean insertion(EmployeeDetails employeeDetails,EmployeeAddress address)
	{
		//Fetching connection object from util package
		Connection con = DBConnection.getDBConnection();
		boolean flag=false;
		try {
			 Statement st = con.createStatement();
			 String sqladdress = "insert into address_employee_jsp(city,state,country) values(?,?,?)";
			 PreparedStatement preparedStatement = con.prepareStatement(sqladdress);
			 preparedStatement.setString(1, address.getCity());
			 preparedStatement.setString(2, address.getState());
			 preparedStatement.setString(3, address.getCountry());
			 int rows = preparedStatement.executeUpdate();
			 ResultSet rs = st.executeQuery("select max(addressid) from address_employee_jsp");
			 while(rs.next())
			 {
				 address.setId(rs.getInt("max(addressid)"));
			 }
			 employeeDetails.setAddress(address);
			 String sqlemployee = "insert into employee_details_jsp(emp_name,emp_age,emp_address) values(?,?,?)";
			 preparedStatement = con.prepareStatement(sqlemployee);
			 preparedStatement.setString(1, employeeDetails.getEmp_name());
			 preparedStatement.setInt(2, employeeDetails.getEmp_age());
			 preparedStatement.setInt(3, address.getId());
			 //executing query to insert an employee to the database
			 rows = preparedStatement.executeUpdate();
			 if(rows > 0)
			 {
				 flag=true;
			 }
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			try {
				//closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
}
