package com.employeeprojectjson.bean;

/**
 * This pojo class contains data about the employee
 * @author Sashank
 *
 */
public class EmployeeDetails {
	//declaring variables
	private String emp_name;
	private int emp_age;
	private int emp_id;
	private EmployeeAddress address;
	
	//generating getters and setters methods for parameters in the pojo class
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	public int getEmp_age() {
		return emp_age;
	}
	public void setEmp_age(int emp_age) {
		this.emp_age = emp_age;
	}
	public int getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}
	public EmployeeAddress getAddress() {
		return address;
	}
	public void setAddress(EmployeeAddress address) {
		this.address = address;
	}
	
}
