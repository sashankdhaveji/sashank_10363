package com.retailbillingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.util.DBUtil;

/**
 * This class is used to perform Products DB operations
 * @author Batch-A
 *
 */
public class ProductsDao {
	/**
	 * This method is used to fetch products
	 * @return list of products
	 */
	public static List<Products> readFromTable_products()
	{
		Connection connection=DBUtil.getconn();
		ResultSet resultSet=null;
		List<Products> products = new ArrayList<Products>();
		try {
			//initializing statement object
			Statement statement=connection.createStatement();
			//calling method to execute query
			resultSet=statement.executeQuery("select * from Products_tab");
			//adding details fetched from database to the list
			while(resultSet.next())
			{
				Products product = new Products();
				product.setProduct_id(resultSet.getInt(1));
				product.setProductname(resultSet.getString(2));
				product.setQuantity(resultSet.getDouble(3));
				product.setPrice(resultSet.getDouble(4));
				product.setProductType(resultSet.getString(5));
				products.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//returning list
		return products;
		
	}
	
	/**
	 * This method is used to fetch billing products
	 * @return list of products
	 */
	public static List<Products> readFromTable_billingProducts(int invoice_number)
	{
		Connection connection=DBUtil.getconn();
		ResultSet resultSet=null;
		List<Products> billingProducts = new ArrayList<Products>();
		try {
			String sql="select  distinct  p.product_name, E.product_price,E.product_quantity from products_tab p, invoice_tab  E where E.Invoicenumber=? and p.product_id=E.product_id";
			//initializing statement object
			PreparedStatement statement=connection.prepareStatement(sql);
			statement.setInt(1, invoice_number);
			//calling method to execute query
			resultSet=statement.executeQuery();
			//adding details fetched from database to the list
			while(resultSet.next())
			{
				Products product = new Products();
				product.setProductname(resultSet.getString(1));
				product.setQuantity(resultSet.getDouble(3));
				product.setPrice(resultSet.getDouble(2));
				billingProducts.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//returning list
		return billingProducts;
	}
	
	
	/**
	 * This method is used for deleting the product when required. This is a stocker operation.
	 * @param productName
	 * @return true if product deleted successfully else false
	 */
	public boolean deleteFromTable_products(String productName) {
		
		//calling a method to establish connection
		Connection connection = DBUtil.getconn();
		try {
			//executing the delete query using prepared statement
			String sql = "delete from Products_tab where product_name=?";
			
			PreparedStatement pst = connection.prepareStatement(sql);
			
			//setting the values into table
			pst.setString(1, productName);
			int i = pst.executeUpdate();
			if (i == 1) {
				return true;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * This method is used for updating quantity of a particular product
	 * @param productName
	 * @param quantity
	 * @return true if product's quantity updated successfully else false
	 */
	public boolean updateTable_products_quantity(String productName, double quantity) {
		//calling a method to establish connection
		Connection connection = DBUtil.getconn();
	
		
		try {

			//executing the update query using prepared statement
			String sql = "update products_tab set product_quantity = ? where product_name = ?";
			
			PreparedStatement pst = connection.prepareStatement(sql);
						
			//setting the values into table
			pst.setDouble(1, quantity);
			pst.setString(2, productName);
			
			//storing the no of rows effected value
			int rowsEffected = pst.executeUpdate();
			if (rowsEffected == 1) {
				return true;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}
	/**
	 * This method is used for updating price of a particular product
	 * @param productName
	 * @param price
	 * @return true if product's price updated successfully else false
	 */
	public boolean updateTable_products_price(String productName, double price) {
		//calling a method to establish connection
		Connection connection = DBUtil.getconn();
	
		
		try {

			//executing the update query using prepared statement
			String sql = "update products_tab set product_price = ? where product_name = ?";
			
			PreparedStatement pst = connection.prepareStatement(sql);
			
			//setting the values into table
			pst.setDouble(1, price);
			pst.setString(2, productName);
			
			//storing the no of rows effected value
			int rowsEffected = pst.executeUpdate();
			if (rowsEffected == 1) {
				return true;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}
	
	/**
	 * This is used for inserting product data into table. This is a stocker operation
	 * @param productname
	 * @param quantity
	 * @param price
	 * @param productType
	 */
	public boolean productsInsertion(Products products)
	{
		Connection con = null;
		boolean flag=false;
		try {
			//calling the method to establish connection
			con = DBUtil.getconn();
			
			//executing insert query by using prepared statement 
			String sql="insert into Products_tab(product_name,product_quantity,product_price,quantity_type)values(?,?,?,?)";
			PreparedStatement pst= con.prepareStatement(sql);
			
			//setting the values into table
			pst.setString(1, products.getProductname());
			pst.setDouble(2, products.getQuantity());
			pst.setDouble(3, products.getPrice());
			pst.setString(4, products.getProductType());
			pst.executeUpdate();
			flag=true;
						
		} catch (SQLException e) {
			
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return flag;
	}
	/**
	 * This is used for inserting product data into table while generating the bill. This is a biller operation.
	 * @param invoiceList
	 */
	public void billingProductsInsertion(ArrayList<Invoice_details> invoiceList) {
		
		Connection con = null;
		for(Invoice_details details:invoiceList) {
			try {
				//calling the method to establish connection
				con = DBUtil.getconn();
				
				//executing insert query by using prepared statement 
				String sql="insert into invoice_tab values(?,?,?,?)";
				PreparedStatement pst= con.prepareStatement(sql);
				
				//setting the values into table
				pst.setInt(1, details.getInvoiceno());
				pst.setInt(2, details.getProduct_id());
				pst.setDouble(3, details.getQuantity());
				pst.setDouble(4, details.getPrice());
				pst.executeUpdate();
							
			} catch (SQLException e) {
				e.printStackTrace();
			}finally {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
