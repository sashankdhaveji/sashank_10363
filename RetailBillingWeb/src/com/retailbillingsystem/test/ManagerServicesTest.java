package com.retailbillingsystem.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.services.ManagerService;

import jdk.nashorn.internal.ir.annotations.Ignore;

/**
 * This class is created to test all possible scenarios in manager services
 * @author Batch A
 *
 */
public class ManagerServicesTest {
	
	ManagerService managerservice = null;
	
	@Before
	public void setUp()
	{
		managerservice = new ManagerService();
	}
	
	@After
	public void tearDown()
	{
		managerservice = null;
	}
	
	/**
	 * This method is used to test the employee adding feature in manager services
	 */
	@Test
	public void createEmployeeTestPositive()
	{
		EmployeeDetails details = new EmployeeDetails();
		details.setEmployeeName("EmployeeTest");
		details.setEmployeePassword("EmployeeTest");
		details.setRole("Stocker");
		details.setSecurityQuestion("TestDish");
		
		assertTrue(managerservice.createEmployee(details));
	}
	
	/**
	 * This method is used to test the employee deletion feature in manager services
	 */
	@Ignore
	@Test
	public void deleteEmployeeTestPositive()
	{
		assertTrue(managerservice.deleteEmployee(107,"Stocker"));
	}
	
	/**
	 * This method is used to test the employee deletion feature in manager services
	 */
	@Test
	public void deleteEmployeeTestNegative()
	{
		assertFalse(managerservice.deleteEmployee(1010,"TestRole"));
	}
	
	/**
	 * This method is used to test the employee display feature in manager services
	 */
	@Test
	public void displayEmployeesTest()
	{
		List<EmployeeDetails> employeeDetails = managerservice.displayEmployees();
		assertFalse(employeeDetails.isEmpty());
	}
}
