package com.retailbillingsystem.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.ProductsDao;
import com.retailbillingsystem.services.StockerServices;

import jdk.nashorn.internal.ir.annotations.Ignore;


public class StockerTest {
  
  StockerServices services=null;
  

	@Before
	public void setUp() {
		services=new StockerServices();
		}
	
	@After
	public void tearDown() {
		services=null;
		
		}
	@Test
	public void addProductTest()
	{
		StockerServices services=new StockerServices();
		Products product=new Products();
		product.setProductname("mida");
		product.setProductType("weights");
		product.setPrice(15);
		
		
		assertTrue(services.addProduct(product));
	}


	@Test
	public  void displayProductsTest()
	{
	
		List<Products> products=ProductsDao.readFromTable_products();
		assertFalse(products.isEmpty());
	}
	
	
	
	@Ignore
	@Test
	public void  deleteProductTest()
	{
		
		ProductsDao delete=new ProductsDao();
		assertTrue(delete.deleteFromTable_products("mida")); 
    }
	
	@Test
	public void updateProductQuantityWithIncreaseTest()
	{
		StockerServices services=new  StockerServices();
			String result=services.updateProductQuantity("banana",2,"1");
			assertTrue(result.equals("Product Updated Successfully......"));
			
			
	}
	@Test
	public void updateProductQuantityWithDecreaseTest()
	{
		StockerServices services=new  StockerServices();
			String result=services.updateProductQuantity("lemon",2.5,"2");
			assertTrue(result.equals("Product Updated Successfully......"));
			
			
	}
	public void  updateProductPriceWithIncreaseTest()
	{
		StockerServices services=new  StockerServices();
		String result=services. updateProductPrice("banana",120.5,"1");
		assertTrue(result.equals("Product Updated Successfully......"));
	}
	
	@Test
	public void  updateProductPricewithDecreaseTest()
	{
		StockerServices services=new  StockerServices();
			String result=services. updateProductPrice("lemon",1.5,"2");
			assertTrue(result.equals("Product Updated Successfully......"));
			
			
	}
	
	
}
