package com.retailbillingsystem.util;

import java.util.List;
import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.dao.EmployeeDao;
/**
 * This method is used for getting password details
 * @author Batch-A
 *
 */
public class ForgotUtilites {
    /**
     * This method is used for getting password details from database table
     * @param id
     * @param dish
     * @param role
     * @return password 
     */
	public static String forgotPasswod(EmployeeDetails details)
	{
		String password="";
		List<EmployeeDetails> employeeDetails = EmployeeDao.fetchEmployeeDetails();
		// checking details and getting password from table
		for(EmployeeDetails  employee: employeeDetails)
		{
			if((details.getEmployeeId()==employee.getEmployeeId())&&(details.getSecurityQuestion().equals(employee.getSecurityQuestion())&&(employee.getRole().equalsIgnoreCase(details.getRole())))) {
			     password=employee.getEmployeePassword();
			}
			
			
		}
		
		return password;
		
	}
}