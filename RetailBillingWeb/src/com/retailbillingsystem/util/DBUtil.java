package com.retailbillingsystem.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * This class contains is useful for connecting with database 
 * @author Batch-A
 *
 */
public class DBUtil {
	/**
	 * This method consists of jdbc connections to connect with database 
	 * @return connection object
	 */
	public static   Connection getconn()
	{
		// driver loading and connection establishment 
		Connection connection=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		 connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/retailbilling","root","innominds");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
		
	}

}
