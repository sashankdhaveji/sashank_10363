package com.retailbillingsystem.services;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.retailbillingsystem.bean.FeedBack;
import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.FeedbackDao;
import com.retailbillingsystem.dao.InvoiceDao;
import com.retailbillingsystem.dao.ProductsDao;

/**
 * This class is used to provide services for the biller
 * @author Batch-A
 *
 */
public class BillerServices {
	/**
	 * This method is used to calculate price of teh product with given quantity
	 * @param productname
	 * @param quantity
	 * @return price
	 */
	public double price_calculation(String productname , double quantity)
	{
		List<Products> productList = ProductsDao.readFromTable_products();
		
		double price=0;
		
		for (Products product : productList) {
			if (productname.equalsIgnoreCase(product.getProductname())) {
				price = product.getPrice()*quantity;
			}
		}

		return price;
	}
	/**
	 * This method is used to calculate total price of all the products
	 * @param price
	 * @param total
	 * @return total
	 */
	public double total_calculation(double price , double total)
	{
		total = total+price;
		return total;
	}
	
	/**
	 * This method is used to calculate tax by given total 
	 * @param total
	 * @return tax
	 */
	public double tax_caluculation(double total)
	{
		double tax = ((double)5/100) * total;
		return tax;
	}
	
	/**
	 * This method is used to calculate discount by given total
	 * @param total
	 * @return discount
	 */
	public double discount_calculation(double total)
	{
		double discount = 0;
		if(total >= 500)
		{
			discount = ((double)7/100) * total;
		}
		return discount;
	}
	
	/**
	 * This method will invoke the products insertion method in Writingdata class
	 * @param invoiceList
	 */
	public void billingProducts (ArrayList<Invoice_details> invoiceList) {
		
		ProductsDao data = new ProductsDao();
		data.billingProductsInsertion(invoiceList);
	}
	
	/**
	 * This method will invoke the method invoice details which stores previous bills
	 * @param invoice_num
	 * @param total
	 * @param discount
	 * @param tax
	 */
	public void invoiceStoring (int invoice_num,double total,double discount,double tax) {
		
		InvoiceDao data = new InvoiceDao();
		data.invoiceDetails(invoice_num,total,discount,tax);
	}
	
	/**
	 * This method will invoke feedback details method which stores feedback to database
	 * @param customerfeedback
	 */
	public  static void customerFeedback(FeedBack customerfeedback) {
		FeedbackDao data = new FeedbackDao();
		data.feedbackDetails(customerfeedback);
	}
	
	/***
	 * This method is used to validate mobile number
	 * @param mobilenumber
	 * @return true if format is correct
	 */
	public boolean mobileCheck(String mobilenumber) { 
//	         1) Begins with 0 or 91 
//	         2) Then contains 7 or 8 or 9. 
//	         3) Then contains 9 digits 
	        Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");
	        Matcher m = p.matcher(mobilenumber); 
	        return (m.find() && m.group().equals(mobilenumber)); 
	    } 
}
