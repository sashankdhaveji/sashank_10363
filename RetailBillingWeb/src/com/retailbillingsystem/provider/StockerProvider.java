package com.retailbillingsystem.provider;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.services.StockerServices;
/**
 * This class is used to provide implementations to stocker
 * @author BatchA
 *
 */
@Path("/stocker")
public class StockerProvider{
	//initializing json object
		private Gson gson = new Gson();
	
	@Path("/addProducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	/**
	 * this method is used to add prodcuts to products table
	 * @param products
	 * @return String
	 */
	public String addProducts(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		//calling the add product method to add the product
		boolean res = stockerServices.addProduct(products);
		return (res ? "Added Successfully" : "Addition failed");
	}
	
	/**
	 *  this method is used to delete products from products table 
	 * @param products
	 * @return String
	 */
	@GET
	@Path("/delProducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String delProducts(Products products) {
		
		StockerServices stockerServices = new StockerServices();
		//calling the delete product method to delete the product
		boolean res = stockerServices.deleteProduct(products.getProductname());
		return (res ? "Deleted Successfully" : "Deletion failed");
		
	}
	/**
	 * This method is used to display existing products
	 * @return List of products
	 */
	@GET
	@Path("/displayProducts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Products> displayProducts()
	{
		StockerServices stockerServices = new StockerServices();
		//calling the display product method to display the product details
		List<Products> productList = stockerServices.displayProducts();
		//returning the productlist details
		return productList;
	}
	
	/**
	 * This method is used to increase quantity of the existing product
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/increaseQuantity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String increaseQuantity(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		String productJsonString ="";
		//calling the update product method to update the product details
		String result = stockerServices.updateProductQuantity(products.getProductname(), products.getQuantity(), "1");
		productJsonString = gson.toJson(result);//converting the result object to json
		return productJsonString;
	}
	
	/**
	 * This method is used to decrease quantity of the existing product 
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/decreaseQuantity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String decreaseQuantity(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		String productJsonString ="";
		//calling the update product method to update the product details
		String result = stockerServices.updateProductQuantity(products.getProductname(), products.getQuantity(), "2");
		productJsonString = gson.toJson(result);
		return productJsonString;
	}
	
	/**
	 * This method is used to increase price of the existing product
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/increasePrice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String increasePrice(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		String productJsonString ="";
		//calling the update product method to update the product details
		String result = stockerServices.updateProductPrice(products.getProductname(),products.getPrice(), "1");
		productJsonString = gson.toJson(result);
		return productJsonString;
	}
	
	/**
	 * This method is used to decrease price of the existing product 
	 * @param products
	 * @return String
	 */
	@PUT
	@Path("/decreasePrice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String decreasePrice(Products products)
	{
		StockerServices stockerServices = new StockerServices();
		String productJsonString ="";
		//calling the update product method to update the product details
		String result = stockerServices.updateProductPrice(products.getProductname(),products.getPrice(), "2");
		productJsonString = gson.toJson(result);
		return productJsonString;
	}
}